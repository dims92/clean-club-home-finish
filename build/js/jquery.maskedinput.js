(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {

var ua = navigator.userAgent,
	iPhone = /iphone/i.test(ua),
	chrome = /chrome/i.test(ua),
	android = /android/i.test(ua),
	caretTimeoutId;

$.mask = {
	//Predefined character definitions
	definitions: {
		'9': "[0-9]",
		'a': "[A-Za-z]",
		'*': "[A-Za-z0-9]"
	},
	autoclear: true,
	dataName: "rawMaskFn",
	placeholder: '_'
};

$.fn.extend({
	//Helper Function for Caret positioning
	caret: function(begin, end) {
		var range;

		if (this.length === 0 || this.is(":hidden") || this.get(0) !== document.activeElement) {
			return;
		}

		if (typeof begin == 'number') {
			end = (typeof end === 'number') ? end : begin;
			return this.each(function() {
				if (this.setSelectionRange) {
					this.setSelectionRange(begin, end);
				} else if (this.createTextRange) {
					range = this.createTextRange();
					range.collapse(true);
					range.moveEnd('character', end);
					range.moveStart('character', begin);
					range.select();
				}
			});
		} else {
			if (this[0].setSelectionRange) {
				begin = this[0].selectionStart;
				end = this[0].selectionEnd;
			} else if (document.selection && document.selection.createRange) {
				range = document.selection.createRange();
				begin = 0 - range.duplicate().moveStart('character', -100000);
				end = begin + range.text.length;
			}
			return { begin: begin, end: end };
		}
	},
	unmask: function() {
		return this.trigger("unmask");
	},
	mask: function(mask, settings) {
		var input,
			defs,
			tests,
			partialPosition,
			firstNonMaskPos,
            lastRequiredNonMaskPos,
            len,
            oldVal;

		if (!mask && this.length > 0) {
			input = $(this[0]);
            var fn = input.data($.mask.dataName)
			return fn?fn():undefined;
		}

		settings = $.extend({
			autoclear: $.mask.autoclear,
			placeholder: $.mask.placeholder, // Load default placeholder
			completed: null
		}, settings);


		defs = $.mask.definitions;
		tests = [];
		partialPosition = len = mask.length;
		firstNonMaskPos = null;

		mask = String(mask);

		$.each(mask.split(""), function(i, c) {
			if (c == '?') {
				len--;
				partialPosition = i;
			} else if (defs[c]) {
				tests.push(new RegExp(defs[c]));
				if (firstNonMaskPos === null) {
					firstNonMaskPos = tests.length - 1;
				}
                if(i < partialPosition){
                    lastRequiredNonMaskPos = tests.length - 1;
                }
			} else {
				tests.push(null);
			}
		});

		return this.trigger("unmask").each(function() {
			var input = $(this),
				buffer = $.map(
    				mask.split(""),
    				function(c, i) {
    					if (c != '?') {
    						return defs[c] ? getPlaceholder(i) : c;
    					}
    				}),
				defaultBuffer = buffer.join(''),
				focusText = input.val();

            function tryFireCompleted(){
                if (!settings.completed) {
                    return;
                }

                for (var i = firstNonMaskPos; i <= lastRequiredNonMaskPos; i++) {
                    if (tests[i] && buffer[i] === getPlaceholder(i)) {
                        return;
                    }
                }
                settings.completed.call(input);
            }

            function getPlaceholder(i){
                if(i < settings.placeholder.length)
                    return settings.placeholder.charAt(i);
                return settings.placeholder.charAt(0);
            }

			function seekNext(pos) {
				while (++pos < len && !tests[pos]);
				return pos;
			}

			function seekPrev(pos) {
				while (--pos >= 0 && !tests[pos]);
				return pos;
			}

			function shiftL(begin,end) {
				var i,
					j;

				if (begin<0) {
					return;
				}

				for (i = begin, j = seekNext(end); i < len; i++) {
					if (tests[i]) {
						if (j < len && tests[i].test(buffer[j])) {
							buffer[i] = buffer[j];
							buffer[j] = getPlaceholder(j);
						} else {
							break;
						}

						j = seekNext(j);
					}
				}
				writeBuffer();
				input.caret(Math.max(firstNonMaskPos, begin));
			}

			function shiftR(pos) {
				var i,
					c,
					j,
					t;

				for (i = pos, c = getPlaceholder(pos); i < len; i++) {
					if (tests[i]) {
						j = seekNext(i);
						t = buffer[i];
						buffer[i] = c;
						if (j < len && tests[j].test(t)) {
							c = t;
						} else {
							break;
						}
					}
				}
			}

			function androidInputEvent(e) {
				var curVal = input.val();
				var pos = input.caret();
				if (oldVal && oldVal.length && oldVal.length > curVal.length ) {
					// a deletion or backspace happened
					checkVal(true);
					while (pos.begin > 0 && !tests[pos.begin-1])
						pos.begin--;
					if (pos.begin === 0)
					{
						while (pos.begin < firstNonMaskPos && !tests[pos.begin])
							pos.begin++;
					}
					input.caret(pos.begin,pos.begin);
				} else {
					var pos2 = checkVal(true);
					var lastEnteredValue = curVal.charAt(pos.begin);
					if (pos.begin < len){
						if(!tests[pos.begin]){
							pos.begin++;
							if(tests[pos.begin].test(lastEnteredValue)){
								pos.begin++;
							}
						}else{
							if(tests[pos.begin].test(lastEnteredValue)){
								pos.begin++;
							}
						}
					}
					input.caret(pos.begin,pos.begin);
				}
				tryFireCompleted();
			}


			function blurEvent(e) {
                checkVal();

                if (input.val() != focusText)
                    input.change();
            }

			function keydownEvent(e) {
                if (input.prop("readonly")){
                    return;
                }

				var k = e.which || e.keyCode,
					pos,
					begin,
					end;
                    oldVal = input.val();
				//backspace, delete, and escape get special treatment
				if (k === 8 || k === 46 || (iPhone && k === 127)) {
					pos = input.caret();
					begin = pos.begin;
					end = pos.end;

					if (end - begin === 0) {
						begin=k!==46?seekPrev(begin):(end=seekNext(begin-1));
						end=k===46?seekNext(end):end;
					}
					clearBuffer(begin, end);
					shiftL(begin, end - 1);

					e.preventDefault();
				} else if( k === 13 ) { // enter
					blurEvent.call(this, e);
				} else if (k === 27) { // escape
					input.val(focusText);
					input.caret(0, checkVal());
					e.preventDefault();
				}
			}

			function keypressEvent(e) {
                if (input.prop("readonly")){
                    return;
                }

				var k = e.which || e.keyCode,
					pos = input.caret(),
					p,
					c,
					next;

				if (e.ctrlKey || e.altKey || e.metaKey || k < 32) {//Ignore
					return;
				} else if ( k && k !== 13 ) {
					if (pos.end - pos.begin !== 0){
						clearBuffer(pos.begin, pos.end);
						shiftL(pos.begin, pos.end-1);
					}

					p = seekNext(pos.begin - 1);
					if (p < len) {
						c = String.fromCharCode(k);
						if (tests[p].test(c)) {
							shiftR(p);

							buffer[p] = c;
							writeBuffer();
							next = seekNext(p);

							if(android){
								//Path for CSP Violation on FireFox OS 1.1
								var proxy = function() {
									$.proxy($.fn.caret,input,next)();
								};

								setTimeout(proxy,0);
							}else{
								input.caret(next);
							}
                            if(pos.begin <= lastRequiredNonMaskPos){
		                         tryFireCompleted();
                             }
						}
					}
					e.preventDefault();
				}
			}

			function clearBuffer(start, end) {
				var i;
				for (i = start; i < end && i < len; i++) {
					if (tests[i]) {
						buffer[i] = getPlaceholder(i);
					}
				}
			}

			function writeBuffer() { input.val(buffer.join('')); }

			function checkVal(allow) {
				//try to place characters where they belong
				var test = input.val(),
					lastMatch = -1,
					i,
					c,
					pos;

				for (i = 0, pos = 0; i < len; i++) {
					if (tests[i]) {
						buffer[i] = getPlaceholder(i);
						while (pos++ < test.length) {
							c = test.charAt(pos - 1);
							if (tests[i].test(c)) {
								buffer[i] = c;
								lastMatch = i;
								break;
							}
						}
						if (pos > test.length) {
							clearBuffer(i + 1, len);
							break;
						}
					} else {
                        if (buffer[i] === test.charAt(pos)) {
                            pos++;
                        }
                        if( i < partialPosition){
                            lastMatch = i;
                        }
					}
				}
				if (allow) {
					writeBuffer();
				} else if (lastMatch + 1 < partialPosition) {
					if (settings.autoclear || buffer.join('') === defaultBuffer) {
						// Invalid value. Remove it and replace it with the
						// mask, which is the default behavior.
						if(input.val()) input.val("");
						clearBuffer(0, len);
					} else {
						// Invalid value, but we opt to show the value to the
						// user and allow them to correct their mistake.
						writeBuffer();
					}
				} else {
					writeBuffer();
					input.val(input.val().substring(0, lastMatch + 1));
				}
				return (partialPosition ? i : firstNonMaskPos);
			}

			input.data($.mask.dataName,function(){
				return $.map(buffer, function(c, i) {
					return tests[i]&&c!=getPlaceholder(i) ? c : null;
				}).join('');
			});


			input
				.one("unmask", function() {
					input
						.off(".mask")
						.removeData($.mask.dataName);
				})
				.on("focus.mask", function() {
                    if (input.prop("readonly")){
                        return;
                    }

					clearTimeout(caretTimeoutId);
					var pos;

					focusText = input.val();

					pos = checkVal();

					caretTimeoutId = setTimeout(function(){
                        if(input.get(0) !== document.activeElement){
                            return;
                        }
						writeBuffer();
						if (pos == mask.replace("?","").length) {
							input.caret(0, pos);
						} else {
							input.caret(pos);
						}
					}, 10);
				})
				.on("blur.mask", blurEvent)
				.on("keydown.mask", keydownEvent)
				.on("keypress.mask", keypressEvent)
				.on("input.mask paste.mask", function() {
                    if (input.prop("readonly")){
                        return;
                    }

					setTimeout(function() {
						var pos=checkVal(true);
						input.caret(pos);
                        tryFireCompleted();
					}, 0);
				});
                if (chrome && android)
                {
                    input
                        .off('input.mask')
                        .on('input.mask', androidInputEvent);
                }
				checkVal(); //Perform initial check for existing values
		});
	}
});
}));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJqcXVlcnkubWFza2VkaW5wdXQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIChmYWN0b3J5KSB7XG4gICAgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xuICAgICAgICAvLyBBTUQuIFJlZ2lzdGVyIGFzIGFuIGFub255bW91cyBtb2R1bGUuXG4gICAgICAgIGRlZmluZShbJ2pxdWVyeSddLCBmYWN0b3J5KTtcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBleHBvcnRzID09PSAnb2JqZWN0Jykge1xuICAgICAgICAvLyBOb2RlL0NvbW1vbkpTXG4gICAgICAgIGZhY3RvcnkocmVxdWlyZSgnanF1ZXJ5JykpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIEJyb3dzZXIgZ2xvYmFsc1xuICAgICAgICBmYWN0b3J5KGpRdWVyeSk7XG4gICAgfVxufShmdW5jdGlvbiAoJCkge1xuXG52YXIgdWEgPSBuYXZpZ2F0b3IudXNlckFnZW50LFxuXHRpUGhvbmUgPSAvaXBob25lL2kudGVzdCh1YSksXG5cdGNocm9tZSA9IC9jaHJvbWUvaS50ZXN0KHVhKSxcblx0YW5kcm9pZCA9IC9hbmRyb2lkL2kudGVzdCh1YSksXG5cdGNhcmV0VGltZW91dElkO1xuXG4kLm1hc2sgPSB7XG5cdC8vUHJlZGVmaW5lZCBjaGFyYWN0ZXIgZGVmaW5pdGlvbnNcblx0ZGVmaW5pdGlvbnM6IHtcblx0XHQnOSc6IFwiWzAtOV1cIixcblx0XHQnYSc6IFwiW0EtWmEtel1cIixcblx0XHQnKic6IFwiW0EtWmEtejAtOV1cIlxuXHR9LFxuXHRhdXRvY2xlYXI6IHRydWUsXG5cdGRhdGFOYW1lOiBcInJhd01hc2tGblwiLFxuXHRwbGFjZWhvbGRlcjogJ18nXG59O1xuXG4kLmZuLmV4dGVuZCh7XG5cdC8vSGVscGVyIEZ1bmN0aW9uIGZvciBDYXJldCBwb3NpdGlvbmluZ1xuXHRjYXJldDogZnVuY3Rpb24oYmVnaW4sIGVuZCkge1xuXHRcdHZhciByYW5nZTtcblxuXHRcdGlmICh0aGlzLmxlbmd0aCA9PT0gMCB8fCB0aGlzLmlzKFwiOmhpZGRlblwiKSB8fCB0aGlzLmdldCgwKSAhPT0gZG9jdW1lbnQuYWN0aXZlRWxlbWVudCkge1xuXHRcdFx0cmV0dXJuO1xuXHRcdH1cblxuXHRcdGlmICh0eXBlb2YgYmVnaW4gPT0gJ251bWJlcicpIHtcblx0XHRcdGVuZCA9ICh0eXBlb2YgZW5kID09PSAnbnVtYmVyJykgPyBlbmQgOiBiZWdpbjtcblx0XHRcdHJldHVybiB0aGlzLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHRcdGlmICh0aGlzLnNldFNlbGVjdGlvblJhbmdlKSB7XG5cdFx0XHRcdFx0dGhpcy5zZXRTZWxlY3Rpb25SYW5nZShiZWdpbiwgZW5kKTtcblx0XHRcdFx0fSBlbHNlIGlmICh0aGlzLmNyZWF0ZVRleHRSYW5nZSkge1xuXHRcdFx0XHRcdHJhbmdlID0gdGhpcy5jcmVhdGVUZXh0UmFuZ2UoKTtcblx0XHRcdFx0XHRyYW5nZS5jb2xsYXBzZSh0cnVlKTtcblx0XHRcdFx0XHRyYW5nZS5tb3ZlRW5kKCdjaGFyYWN0ZXInLCBlbmQpO1xuXHRcdFx0XHRcdHJhbmdlLm1vdmVTdGFydCgnY2hhcmFjdGVyJywgYmVnaW4pO1xuXHRcdFx0XHRcdHJhbmdlLnNlbGVjdCgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0aWYgKHRoaXNbMF0uc2V0U2VsZWN0aW9uUmFuZ2UpIHtcblx0XHRcdFx0YmVnaW4gPSB0aGlzWzBdLnNlbGVjdGlvblN0YXJ0O1xuXHRcdFx0XHRlbmQgPSB0aGlzWzBdLnNlbGVjdGlvbkVuZDtcblx0XHRcdH0gZWxzZSBpZiAoZG9jdW1lbnQuc2VsZWN0aW9uICYmIGRvY3VtZW50LnNlbGVjdGlvbi5jcmVhdGVSYW5nZSkge1xuXHRcdFx0XHRyYW5nZSA9IGRvY3VtZW50LnNlbGVjdGlvbi5jcmVhdGVSYW5nZSgpO1xuXHRcdFx0XHRiZWdpbiA9IDAgLSByYW5nZS5kdXBsaWNhdGUoKS5tb3ZlU3RhcnQoJ2NoYXJhY3RlcicsIC0xMDAwMDApO1xuXHRcdFx0XHRlbmQgPSBiZWdpbiArIHJhbmdlLnRleHQubGVuZ3RoO1xuXHRcdFx0fVxuXHRcdFx0cmV0dXJuIHsgYmVnaW46IGJlZ2luLCBlbmQ6IGVuZCB9O1xuXHRcdH1cblx0fSxcblx0dW5tYXNrOiBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gdGhpcy50cmlnZ2VyKFwidW5tYXNrXCIpO1xuXHR9LFxuXHRtYXNrOiBmdW5jdGlvbihtYXNrLCBzZXR0aW5ncykge1xuXHRcdHZhciBpbnB1dCxcblx0XHRcdGRlZnMsXG5cdFx0XHR0ZXN0cyxcblx0XHRcdHBhcnRpYWxQb3NpdGlvbixcblx0XHRcdGZpcnN0Tm9uTWFza1BvcyxcbiAgICAgICAgICAgIGxhc3RSZXF1aXJlZE5vbk1hc2tQb3MsXG4gICAgICAgICAgICBsZW4sXG4gICAgICAgICAgICBvbGRWYWw7XG5cblx0XHRpZiAoIW1hc2sgJiYgdGhpcy5sZW5ndGggPiAwKSB7XG5cdFx0XHRpbnB1dCA9ICQodGhpc1swXSk7XG4gICAgICAgICAgICB2YXIgZm4gPSBpbnB1dC5kYXRhKCQubWFzay5kYXRhTmFtZSlcblx0XHRcdHJldHVybiBmbj9mbigpOnVuZGVmaW5lZDtcblx0XHR9XG5cblx0XHRzZXR0aW5ncyA9ICQuZXh0ZW5kKHtcblx0XHRcdGF1dG9jbGVhcjogJC5tYXNrLmF1dG9jbGVhcixcblx0XHRcdHBsYWNlaG9sZGVyOiAkLm1hc2sucGxhY2Vob2xkZXIsIC8vIExvYWQgZGVmYXVsdCBwbGFjZWhvbGRlclxuXHRcdFx0Y29tcGxldGVkOiBudWxsXG5cdFx0fSwgc2V0dGluZ3MpO1xuXG5cblx0XHRkZWZzID0gJC5tYXNrLmRlZmluaXRpb25zO1xuXHRcdHRlc3RzID0gW107XG5cdFx0cGFydGlhbFBvc2l0aW9uID0gbGVuID0gbWFzay5sZW5ndGg7XG5cdFx0Zmlyc3ROb25NYXNrUG9zID0gbnVsbDtcblxuXHRcdG1hc2sgPSBTdHJpbmcobWFzayk7XG5cblx0XHQkLmVhY2gobWFzay5zcGxpdChcIlwiKSwgZnVuY3Rpb24oaSwgYykge1xuXHRcdFx0aWYgKGMgPT0gJz8nKSB7XG5cdFx0XHRcdGxlbi0tO1xuXHRcdFx0XHRwYXJ0aWFsUG9zaXRpb24gPSBpO1xuXHRcdFx0fSBlbHNlIGlmIChkZWZzW2NdKSB7XG5cdFx0XHRcdHRlc3RzLnB1c2gobmV3IFJlZ0V4cChkZWZzW2NdKSk7XG5cdFx0XHRcdGlmIChmaXJzdE5vbk1hc2tQb3MgPT09IG51bGwpIHtcblx0XHRcdFx0XHRmaXJzdE5vbk1hc2tQb3MgPSB0ZXN0cy5sZW5ndGggLSAxO1xuXHRcdFx0XHR9XG4gICAgICAgICAgICAgICAgaWYoaSA8IHBhcnRpYWxQb3NpdGlvbil7XG4gICAgICAgICAgICAgICAgICAgIGxhc3RSZXF1aXJlZE5vbk1hc2tQb3MgPSB0ZXN0cy5sZW5ndGggLSAxO1xuICAgICAgICAgICAgICAgIH1cblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHRlc3RzLnB1c2gobnVsbCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0XHRyZXR1cm4gdGhpcy50cmlnZ2VyKFwidW5tYXNrXCIpLmVhY2goZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgaW5wdXQgPSAkKHRoaXMpLFxuXHRcdFx0XHRidWZmZXIgPSAkLm1hcChcbiAgICBcdFx0XHRcdG1hc2suc3BsaXQoXCJcIiksXG4gICAgXHRcdFx0XHRmdW5jdGlvbihjLCBpKSB7XG4gICAgXHRcdFx0XHRcdGlmIChjICE9ICc/Jykge1xuICAgIFx0XHRcdFx0XHRcdHJldHVybiBkZWZzW2NdID8gZ2V0UGxhY2Vob2xkZXIoaSkgOiBjO1xuICAgIFx0XHRcdFx0XHR9XG4gICAgXHRcdFx0XHR9KSxcblx0XHRcdFx0ZGVmYXVsdEJ1ZmZlciA9IGJ1ZmZlci5qb2luKCcnKSxcblx0XHRcdFx0Zm9jdXNUZXh0ID0gaW5wdXQudmFsKCk7XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIHRyeUZpcmVDb21wbGV0ZWQoKXtcbiAgICAgICAgICAgICAgICBpZiAoIXNldHRpbmdzLmNvbXBsZXRlZCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IGZpcnN0Tm9uTWFza1BvczsgaSA8PSBsYXN0UmVxdWlyZWROb25NYXNrUG9zOyBpKyspIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRlc3RzW2ldICYmIGJ1ZmZlcltpXSA9PT0gZ2V0UGxhY2Vob2xkZXIoaSkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBzZXR0aW5ncy5jb21wbGV0ZWQuY2FsbChpbnB1dCk7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGZ1bmN0aW9uIGdldFBsYWNlaG9sZGVyKGkpe1xuICAgICAgICAgICAgICAgIGlmKGkgPCBzZXR0aW5ncy5wbGFjZWhvbGRlci5sZW5ndGgpXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBzZXR0aW5ncy5wbGFjZWhvbGRlci5jaGFyQXQoaSk7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHNldHRpbmdzLnBsYWNlaG9sZGVyLmNoYXJBdCgwKTtcbiAgICAgICAgICAgIH1cblxuXHRcdFx0ZnVuY3Rpb24gc2Vla05leHQocG9zKSB7XG5cdFx0XHRcdHdoaWxlICgrK3BvcyA8IGxlbiAmJiAhdGVzdHNbcG9zXSk7XG5cdFx0XHRcdHJldHVybiBwb3M7XG5cdFx0XHR9XG5cblx0XHRcdGZ1bmN0aW9uIHNlZWtQcmV2KHBvcykge1xuXHRcdFx0XHR3aGlsZSAoLS1wb3MgPj0gMCAmJiAhdGVzdHNbcG9zXSk7XG5cdFx0XHRcdHJldHVybiBwb3M7XG5cdFx0XHR9XG5cblx0XHRcdGZ1bmN0aW9uIHNoaWZ0TChiZWdpbixlbmQpIHtcblx0XHRcdFx0dmFyIGksXG5cdFx0XHRcdFx0ajtcblxuXHRcdFx0XHRpZiAoYmVnaW48MCkge1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fVxuXG5cdFx0XHRcdGZvciAoaSA9IGJlZ2luLCBqID0gc2Vla05leHQoZW5kKTsgaSA8IGxlbjsgaSsrKSB7XG5cdFx0XHRcdFx0aWYgKHRlc3RzW2ldKSB7XG5cdFx0XHRcdFx0XHRpZiAoaiA8IGxlbiAmJiB0ZXN0c1tpXS50ZXN0KGJ1ZmZlcltqXSkpIHtcblx0XHRcdFx0XHRcdFx0YnVmZmVyW2ldID0gYnVmZmVyW2pdO1xuXHRcdFx0XHRcdFx0XHRidWZmZXJbal0gPSBnZXRQbGFjZWhvbGRlcihqKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXG5cdFx0XHRcdFx0XHRqID0gc2Vla05leHQoaik7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cdFx0XHRcdHdyaXRlQnVmZmVyKCk7XG5cdFx0XHRcdGlucHV0LmNhcmV0KE1hdGgubWF4KGZpcnN0Tm9uTWFza1BvcywgYmVnaW4pKTtcblx0XHRcdH1cblxuXHRcdFx0ZnVuY3Rpb24gc2hpZnRSKHBvcykge1xuXHRcdFx0XHR2YXIgaSxcblx0XHRcdFx0XHRjLFxuXHRcdFx0XHRcdGosXG5cdFx0XHRcdFx0dDtcblxuXHRcdFx0XHRmb3IgKGkgPSBwb3MsIGMgPSBnZXRQbGFjZWhvbGRlcihwb3MpOyBpIDwgbGVuOyBpKyspIHtcblx0XHRcdFx0XHRpZiAodGVzdHNbaV0pIHtcblx0XHRcdFx0XHRcdGogPSBzZWVrTmV4dChpKTtcblx0XHRcdFx0XHRcdHQgPSBidWZmZXJbaV07XG5cdFx0XHRcdFx0XHRidWZmZXJbaV0gPSBjO1xuXHRcdFx0XHRcdFx0aWYgKGogPCBsZW4gJiYgdGVzdHNbal0udGVzdCh0KSkge1xuXHRcdFx0XHRcdFx0XHRjID0gdDtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRmdW5jdGlvbiBhbmRyb2lkSW5wdXRFdmVudChlKSB7XG5cdFx0XHRcdHZhciBjdXJWYWwgPSBpbnB1dC52YWwoKTtcblx0XHRcdFx0dmFyIHBvcyA9IGlucHV0LmNhcmV0KCk7XG5cdFx0XHRcdGlmIChvbGRWYWwgJiYgb2xkVmFsLmxlbmd0aCAmJiBvbGRWYWwubGVuZ3RoID4gY3VyVmFsLmxlbmd0aCApIHtcblx0XHRcdFx0XHQvLyBhIGRlbGV0aW9uIG9yIGJhY2tzcGFjZSBoYXBwZW5lZFxuXHRcdFx0XHRcdGNoZWNrVmFsKHRydWUpO1xuXHRcdFx0XHRcdHdoaWxlIChwb3MuYmVnaW4gPiAwICYmICF0ZXN0c1twb3MuYmVnaW4tMV0pXG5cdFx0XHRcdFx0XHRwb3MuYmVnaW4tLTtcblx0XHRcdFx0XHRpZiAocG9zLmJlZ2luID09PSAwKVxuXHRcdFx0XHRcdHtcblx0XHRcdFx0XHRcdHdoaWxlIChwb3MuYmVnaW4gPCBmaXJzdE5vbk1hc2tQb3MgJiYgIXRlc3RzW3Bvcy5iZWdpbl0pXG5cdFx0XHRcdFx0XHRcdHBvcy5iZWdpbisrO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0XHRpbnB1dC5jYXJldChwb3MuYmVnaW4scG9zLmJlZ2luKTtcblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR2YXIgcG9zMiA9IGNoZWNrVmFsKHRydWUpO1xuXHRcdFx0XHRcdHZhciBsYXN0RW50ZXJlZFZhbHVlID0gY3VyVmFsLmNoYXJBdChwb3MuYmVnaW4pO1xuXHRcdFx0XHRcdGlmIChwb3MuYmVnaW4gPCBsZW4pe1xuXHRcdFx0XHRcdFx0aWYoIXRlc3RzW3Bvcy5iZWdpbl0pe1xuXHRcdFx0XHRcdFx0XHRwb3MuYmVnaW4rKztcblx0XHRcdFx0XHRcdFx0aWYodGVzdHNbcG9zLmJlZ2luXS50ZXN0KGxhc3RFbnRlcmVkVmFsdWUpKXtcblx0XHRcdFx0XHRcdFx0XHRwb3MuYmVnaW4rKztcblx0XHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0fWVsc2V7XG5cdFx0XHRcdFx0XHRcdGlmKHRlc3RzW3Bvcy5iZWdpbl0udGVzdChsYXN0RW50ZXJlZFZhbHVlKSl7XG5cdFx0XHRcdFx0XHRcdFx0cG9zLmJlZ2luKys7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0aW5wdXQuY2FyZXQocG9zLmJlZ2luLHBvcy5iZWdpbik7XG5cdFx0XHRcdH1cblx0XHRcdFx0dHJ5RmlyZUNvbXBsZXRlZCgpO1xuXHRcdFx0fVxuXG5cblx0XHRcdGZ1bmN0aW9uIGJsdXJFdmVudChlKSB7XG4gICAgICAgICAgICAgICAgY2hlY2tWYWwoKTtcblxuICAgICAgICAgICAgICAgIGlmIChpbnB1dC52YWwoKSAhPSBmb2N1c1RleHQpXG4gICAgICAgICAgICAgICAgICAgIGlucHV0LmNoYW5nZSgpO1xuICAgICAgICAgICAgfVxuXG5cdFx0XHRmdW5jdGlvbiBrZXlkb3duRXZlbnQoZSkge1xuICAgICAgICAgICAgICAgIGlmIChpbnB1dC5wcm9wKFwicmVhZG9ubHlcIikpe1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG5cdFx0XHRcdHZhciBrID0gZS53aGljaCB8fCBlLmtleUNvZGUsXG5cdFx0XHRcdFx0cG9zLFxuXHRcdFx0XHRcdGJlZ2luLFxuXHRcdFx0XHRcdGVuZDtcbiAgICAgICAgICAgICAgICAgICAgb2xkVmFsID0gaW5wdXQudmFsKCk7XG5cdFx0XHRcdC8vYmFja3NwYWNlLCBkZWxldGUsIGFuZCBlc2NhcGUgZ2V0IHNwZWNpYWwgdHJlYXRtZW50XG5cdFx0XHRcdGlmIChrID09PSA4IHx8IGsgPT09IDQ2IHx8IChpUGhvbmUgJiYgayA9PT0gMTI3KSkge1xuXHRcdFx0XHRcdHBvcyA9IGlucHV0LmNhcmV0KCk7XG5cdFx0XHRcdFx0YmVnaW4gPSBwb3MuYmVnaW47XG5cdFx0XHRcdFx0ZW5kID0gcG9zLmVuZDtcblxuXHRcdFx0XHRcdGlmIChlbmQgLSBiZWdpbiA9PT0gMCkge1xuXHRcdFx0XHRcdFx0YmVnaW49ayE9PTQ2P3NlZWtQcmV2KGJlZ2luKTooZW5kPXNlZWtOZXh0KGJlZ2luLTEpKTtcblx0XHRcdFx0XHRcdGVuZD1rPT09NDY/c2Vla05leHQoZW5kKTplbmQ7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGNsZWFyQnVmZmVyKGJlZ2luLCBlbmQpO1xuXHRcdFx0XHRcdHNoaWZ0TChiZWdpbiwgZW5kIC0gMSk7XG5cblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdH0gZWxzZSBpZiggayA9PT0gMTMgKSB7IC8vIGVudGVyXG5cdFx0XHRcdFx0Ymx1ckV2ZW50LmNhbGwodGhpcywgZSk7XG5cdFx0XHRcdH0gZWxzZSBpZiAoayA9PT0gMjcpIHsgLy8gZXNjYXBlXG5cdFx0XHRcdFx0aW5wdXQudmFsKGZvY3VzVGV4dCk7XG5cdFx0XHRcdFx0aW5wdXQuY2FyZXQoMCwgY2hlY2tWYWwoKSk7XG5cdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cblx0XHRcdGZ1bmN0aW9uIGtleXByZXNzRXZlbnQoZSkge1xuICAgICAgICAgICAgICAgIGlmIChpbnB1dC5wcm9wKFwicmVhZG9ubHlcIikpe1xuICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgfVxuXG5cdFx0XHRcdHZhciBrID0gZS53aGljaCB8fCBlLmtleUNvZGUsXG5cdFx0XHRcdFx0cG9zID0gaW5wdXQuY2FyZXQoKSxcblx0XHRcdFx0XHRwLFxuXHRcdFx0XHRcdGMsXG5cdFx0XHRcdFx0bmV4dDtcblxuXHRcdFx0XHRpZiAoZS5jdHJsS2V5IHx8IGUuYWx0S2V5IHx8IGUubWV0YUtleSB8fCBrIDwgMzIpIHsvL0lnbm9yZVxuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fSBlbHNlIGlmICggayAmJiBrICE9PSAxMyApIHtcblx0XHRcdFx0XHRpZiAocG9zLmVuZCAtIHBvcy5iZWdpbiAhPT0gMCl7XG5cdFx0XHRcdFx0XHRjbGVhckJ1ZmZlcihwb3MuYmVnaW4sIHBvcy5lbmQpO1xuXHRcdFx0XHRcdFx0c2hpZnRMKHBvcy5iZWdpbiwgcG9zLmVuZC0xKTtcblx0XHRcdFx0XHR9XG5cblx0XHRcdFx0XHRwID0gc2Vla05leHQocG9zLmJlZ2luIC0gMSk7XG5cdFx0XHRcdFx0aWYgKHAgPCBsZW4pIHtcblx0XHRcdFx0XHRcdGMgPSBTdHJpbmcuZnJvbUNoYXJDb2RlKGspO1xuXHRcdFx0XHRcdFx0aWYgKHRlc3RzW3BdLnRlc3QoYykpIHtcblx0XHRcdFx0XHRcdFx0c2hpZnRSKHApO1xuXG5cdFx0XHRcdFx0XHRcdGJ1ZmZlcltwXSA9IGM7XG5cdFx0XHRcdFx0XHRcdHdyaXRlQnVmZmVyKCk7XG5cdFx0XHRcdFx0XHRcdG5leHQgPSBzZWVrTmV4dChwKTtcblxuXHRcdFx0XHRcdFx0XHRpZihhbmRyb2lkKXtcblx0XHRcdFx0XHRcdFx0XHQvL1BhdGggZm9yIENTUCBWaW9sYXRpb24gb24gRmlyZUZveCBPUyAxLjFcblx0XHRcdFx0XHRcdFx0XHR2YXIgcHJveHkgPSBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRcdFx0XHRcdCQucHJveHkoJC5mbi5jYXJldCxpbnB1dCxuZXh0KSgpO1xuXHRcdFx0XHRcdFx0XHRcdH07XG5cblx0XHRcdFx0XHRcdFx0XHRzZXRUaW1lb3V0KHByb3h5LDApO1xuXHRcdFx0XHRcdFx0XHR9ZWxzZXtcblx0XHRcdFx0XHRcdFx0XHRpbnB1dC5jYXJldChuZXh0KTtcblx0XHRcdFx0XHRcdFx0fVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmKHBvcy5iZWdpbiA8PSBsYXN0UmVxdWlyZWROb25NYXNrUG9zKXtcblx0XHQgICAgICAgICAgICAgICAgICAgICAgICAgdHJ5RmlyZUNvbXBsZXRlZCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRmdW5jdGlvbiBjbGVhckJ1ZmZlcihzdGFydCwgZW5kKSB7XG5cdFx0XHRcdHZhciBpO1xuXHRcdFx0XHRmb3IgKGkgPSBzdGFydDsgaSA8IGVuZCAmJiBpIDwgbGVuOyBpKyspIHtcblx0XHRcdFx0XHRpZiAodGVzdHNbaV0pIHtcblx0XHRcdFx0XHRcdGJ1ZmZlcltpXSA9IGdldFBsYWNlaG9sZGVyKGkpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0fVxuXG5cdFx0XHRmdW5jdGlvbiB3cml0ZUJ1ZmZlcigpIHsgaW5wdXQudmFsKGJ1ZmZlci5qb2luKCcnKSk7IH1cblxuXHRcdFx0ZnVuY3Rpb24gY2hlY2tWYWwoYWxsb3cpIHtcblx0XHRcdFx0Ly90cnkgdG8gcGxhY2UgY2hhcmFjdGVycyB3aGVyZSB0aGV5IGJlbG9uZ1xuXHRcdFx0XHR2YXIgdGVzdCA9IGlucHV0LnZhbCgpLFxuXHRcdFx0XHRcdGxhc3RNYXRjaCA9IC0xLFxuXHRcdFx0XHRcdGksXG5cdFx0XHRcdFx0Yyxcblx0XHRcdFx0XHRwb3M7XG5cblx0XHRcdFx0Zm9yIChpID0gMCwgcG9zID0gMDsgaSA8IGxlbjsgaSsrKSB7XG5cdFx0XHRcdFx0aWYgKHRlc3RzW2ldKSB7XG5cdFx0XHRcdFx0XHRidWZmZXJbaV0gPSBnZXRQbGFjZWhvbGRlcihpKTtcblx0XHRcdFx0XHRcdHdoaWxlIChwb3MrKyA8IHRlc3QubGVuZ3RoKSB7XG5cdFx0XHRcdFx0XHRcdGMgPSB0ZXN0LmNoYXJBdChwb3MgLSAxKTtcblx0XHRcdFx0XHRcdFx0aWYgKHRlc3RzW2ldLnRlc3QoYykpIHtcblx0XHRcdFx0XHRcdFx0XHRidWZmZXJbaV0gPSBjO1xuXHRcdFx0XHRcdFx0XHRcdGxhc3RNYXRjaCA9IGk7XG5cdFx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdH1cblx0XHRcdFx0XHRcdGlmIChwb3MgPiB0ZXN0Lmxlbmd0aCkge1xuXHRcdFx0XHRcdFx0XHRjbGVhckJ1ZmZlcihpICsgMSwgbGVuKTtcblx0XHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChidWZmZXJbaV0gPT09IHRlc3QuY2hhckF0KHBvcykpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwb3MrKztcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIGlmKCBpIDwgcGFydGlhbFBvc2l0aW9uKXtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsYXN0TWF0Y2ggPSBpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuXHRcdFx0XHRcdH1cblx0XHRcdFx0fVxuXHRcdFx0XHRpZiAoYWxsb3cpIHtcblx0XHRcdFx0XHR3cml0ZUJ1ZmZlcigpO1xuXHRcdFx0XHR9IGVsc2UgaWYgKGxhc3RNYXRjaCArIDEgPCBwYXJ0aWFsUG9zaXRpb24pIHtcblx0XHRcdFx0XHRpZiAoc2V0dGluZ3MuYXV0b2NsZWFyIHx8IGJ1ZmZlci5qb2luKCcnKSA9PT0gZGVmYXVsdEJ1ZmZlcikge1xuXHRcdFx0XHRcdFx0Ly8gSW52YWxpZCB2YWx1ZS4gUmVtb3ZlIGl0IGFuZCByZXBsYWNlIGl0IHdpdGggdGhlXG5cdFx0XHRcdFx0XHQvLyBtYXNrLCB3aGljaCBpcyB0aGUgZGVmYXVsdCBiZWhhdmlvci5cblx0XHRcdFx0XHRcdGlmKGlucHV0LnZhbCgpKSBpbnB1dC52YWwoXCJcIik7XG5cdFx0XHRcdFx0XHRjbGVhckJ1ZmZlcigwLCBsZW4pO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHQvLyBJbnZhbGlkIHZhbHVlLCBidXQgd2Ugb3B0IHRvIHNob3cgdGhlIHZhbHVlIHRvIHRoZVxuXHRcdFx0XHRcdFx0Ly8gdXNlciBhbmQgYWxsb3cgdGhlbSB0byBjb3JyZWN0IHRoZWlyIG1pc3Rha2UuXG5cdFx0XHRcdFx0XHR3cml0ZUJ1ZmZlcigpO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHR3cml0ZUJ1ZmZlcigpO1xuXHRcdFx0XHRcdGlucHV0LnZhbChpbnB1dC52YWwoKS5zdWJzdHJpbmcoMCwgbGFzdE1hdGNoICsgMSkpO1xuXHRcdFx0XHR9XG5cdFx0XHRcdHJldHVybiAocGFydGlhbFBvc2l0aW9uID8gaSA6IGZpcnN0Tm9uTWFza1Bvcyk7XG5cdFx0XHR9XG5cblx0XHRcdGlucHV0LmRhdGEoJC5tYXNrLmRhdGFOYW1lLGZ1bmN0aW9uKCl7XG5cdFx0XHRcdHJldHVybiAkLm1hcChidWZmZXIsIGZ1bmN0aW9uKGMsIGkpIHtcblx0XHRcdFx0XHRyZXR1cm4gdGVzdHNbaV0mJmMhPWdldFBsYWNlaG9sZGVyKGkpID8gYyA6IG51bGw7XG5cdFx0XHRcdH0pLmpvaW4oJycpO1xuXHRcdFx0fSk7XG5cblxuXHRcdFx0aW5wdXRcblx0XHRcdFx0Lm9uZShcInVubWFza1wiLCBmdW5jdGlvbigpIHtcblx0XHRcdFx0XHRpbnB1dFxuXHRcdFx0XHRcdFx0Lm9mZihcIi5tYXNrXCIpXG5cdFx0XHRcdFx0XHQucmVtb3ZlRGF0YSgkLm1hc2suZGF0YU5hbWUpO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQub24oXCJmb2N1cy5tYXNrXCIsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgICAgICAgICBpZiAoaW5wdXQucHJvcChcInJlYWRvbmx5XCIpKXtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgfVxuXG5cdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KGNhcmV0VGltZW91dElkKTtcblx0XHRcdFx0XHR2YXIgcG9zO1xuXG5cdFx0XHRcdFx0Zm9jdXNUZXh0ID0gaW5wdXQudmFsKCk7XG5cblx0XHRcdFx0XHRwb3MgPSBjaGVja1ZhbCgpO1xuXG5cdFx0XHRcdFx0Y2FyZXRUaW1lb3V0SWQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZihpbnB1dC5nZXQoMCkgIT09IGRvY3VtZW50LmFjdGl2ZUVsZW1lbnQpe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cblx0XHRcdFx0XHRcdHdyaXRlQnVmZmVyKCk7XG5cdFx0XHRcdFx0XHRpZiAocG9zID09IG1hc2sucmVwbGFjZShcIj9cIixcIlwiKS5sZW5ndGgpIHtcblx0XHRcdFx0XHRcdFx0aW5wdXQuY2FyZXQoMCwgcG9zKTtcblx0XHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRcdGlucHV0LmNhcmV0KHBvcyk7XG5cdFx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0fSwgMTApO1xuXHRcdFx0XHR9KVxuXHRcdFx0XHQub24oXCJibHVyLm1hc2tcIiwgYmx1ckV2ZW50KVxuXHRcdFx0XHQub24oXCJrZXlkb3duLm1hc2tcIiwga2V5ZG93bkV2ZW50KVxuXHRcdFx0XHQub24oXCJrZXlwcmVzcy5tYXNrXCIsIGtleXByZXNzRXZlbnQpXG5cdFx0XHRcdC5vbihcImlucHV0Lm1hc2sgcGFzdGUubWFza1wiLCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAgICAgaWYgKGlucHV0LnByb3AoXCJyZWFkb25seVwiKSl7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICAgICAgICAgIH1cblxuXHRcdFx0XHRcdHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG5cdFx0XHRcdFx0XHR2YXIgcG9zPWNoZWNrVmFsKHRydWUpO1xuXHRcdFx0XHRcdFx0aW5wdXQuY2FyZXQocG9zKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyeUZpcmVDb21wbGV0ZWQoKTtcblx0XHRcdFx0XHR9LCAwKTtcblx0XHRcdFx0fSk7XG4gICAgICAgICAgICAgICAgaWYgKGNocm9tZSAmJiBhbmRyb2lkKVxuICAgICAgICAgICAgICAgIHtcbiAgICAgICAgICAgICAgICAgICAgaW5wdXRcbiAgICAgICAgICAgICAgICAgICAgICAgIC5vZmYoJ2lucHV0Lm1hc2snKVxuICAgICAgICAgICAgICAgICAgICAgICAgLm9uKCdpbnB1dC5tYXNrJywgYW5kcm9pZElucHV0RXZlbnQpO1xuICAgICAgICAgICAgICAgIH1cblx0XHRcdFx0Y2hlY2tWYWwoKTsgLy9QZXJmb3JtIGluaXRpYWwgY2hlY2sgZm9yIGV4aXN0aW5nIHZhbHVlc1xuXHRcdH0pO1xuXHR9XG59KTtcbn0pKTtcbiJdLCJmaWxlIjoianF1ZXJ5Lm1hc2tlZGlucHV0LmpzIn0=
