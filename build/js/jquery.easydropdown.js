/*
* EASYDROPDOWN - A Drop-down Builder for Styleable Inputs and Menus
* Version: 2.1.4
* License: Creative Commons Attribution 3.0 Unported - CC BY 3.0
* http://creativecommons.org/licenses/by/3.0/
* This software may be used freely on commercial and non-commercial projects with attribution to the author/copyright holder.
* Author: Patrick Kunka
* Copyright 2013 Patrick Kunka, All Rights Reserved
*/


(function($){
	
	function EasyDropDown(){
		this.isField = true,
		this.down = false,
		this.inFocus = false,
		this.disabled = false,
		this.cutOff = false,
		this.hasLabel = false,
		this.keyboardMode = false,
		this.nativeTouch = true,
		this.wrapperClass = 'dropdown',
		this.onChange = null;
	};
	
	EasyDropDown.prototype = {
		constructor: EasyDropDown,
		instances: {},
		init: function(domNode, settings){
			var	self = this;
			
			$.extend(self, settings);
			self.$select = $(domNode);
			self.id = domNode.id;
			self.options = [];
			self.$options = self.$select.find('option');
			self.isTouch = 'ontouchend' in document;
			self.$select.removeClass(self.wrapperClass+' dropdown');
			if(self.$select.is(':disabled')){
				self.disabled = true;
			};
			if(self.$options.length){
				self.$options.each(function(i){
					var $option = $(this);
					if($option.is(':selected')){
						self.selected = {
							index: i,
							title: $option.text()
						}
						self.focusIndex = i;
					};
					if($option.hasClass('label') && i == 0){
						self.hasLabel = true;
						self.label = $option.text();
						$option.attr('value','');
					} else {
						self.options.push({
							domNode: $option[0],
							title: $option.text(),
							value: $option.val(),
							selected: $option.is(':selected')
						});
					};
				});
				if(!self.selected){
					self.selected = {
						index: 0,
						title: self.$options.eq(0).text()
					}
					self.focusIndex = 0;
				};
				self.render();
			};
		},
	
		render: function(){
			var	self = this,
				touchClass = self.isTouch && self.nativeTouch ? ' touch' : '',
				disabledClass = self.disabled ? ' disabled' : '';
			
			self.$container = self.$select.wrap('<div class="'+self.wrapperClass+touchClass+disabledClass+'"><span class="old"/></div>').parent().parent();
			self.$active = $('<span class="selected">'+self.selected.title+'</span>').appendTo(self.$container);
			self.$carat = $('<span class="carat"/>').appendTo(self.$container);
			self.$scrollWrapper = $('<div><ul/></div>').appendTo(self.$container);
			self.$dropDown = self.$scrollWrapper.find('ul');
			self.$form = self.$container.closest('form');
			$.each(self.options, function(){
				var	option = this,
					active = option.selected ? ' class="active"':'';
				self.$dropDown.append('<li'+active+'>'+option.title+'</li>');
			});
			self.$items = self.$dropDown.find('li');
			
			if(self.cutOff && self.$items.length > self.cutOff)self.$container.addClass('scrollable');
			
			self.getMaxHeight();
	
			if(self.isTouch && self.nativeTouch){
				self.bindTouchHandlers();
			} else {
				self.bindHandlers();
			};
		},
		
		getMaxHeight: function(){
			var self = this;
			
			self.maxHeight = 0;
			
			for(i = 0; i < self.$items.length; i++){
				var $item = self.$items.eq(i);
				self.maxHeight += $item.outerHeight();
				if(self.cutOff == i+1){
					break;
				};
			};
		},
		
		bindTouchHandlers: function(){
			var	self = this;
			self.$container.on('click.easyDropDown',function(){
				self.$select.focus();
			});
			self.$select.on({
				change: function(){
					var	$selected = $(this).find('option:selected'),
						title = $selected.text(),
						value = $selected.val();
						
					self.$active.text(title);
					if(typeof self.onChange === 'function'){
						self.onChange.call(self.$select[0],{
							title: title, 
							value: value
						});
					};
				},
				focus: function(){
					self.$container.addClass('focus');
				},
				blur: function(){
					self.$container.removeClass('focus');
				}
			});
		},
	
		bindHandlers: function(){
			var	self = this;
			self.query = '';
			self.$container.on({
				'click.easyDropDown': function(){
					if(!self.down && !self.disabled){
						self.open();
					} else {
						self.close();
					};
				},
				'mousemove.easyDropDown': function(){
					if(self.keyboardMode){
						self.keyboardMode = false;
					};
				}
			});
			
			$('body').on('click.easyDropDown.'+self.id,function(e){
				var $target = $(e.target),
					classNames = self.wrapperClass.split(' ').join('.');

				if(!$target.closest('.'+classNames).length && self.down){
					self.close();
				};
			});

			self.$items.on({
				'click.easyDropDown': function(){
					var index = $(this).index();
					self.select(index);
					self.$select.focus();
				},
				'mouseover.easyDropDown': function(){
					if(!self.keyboardMode){
						var $t = $(this);
						$t.addClass('focus').siblings().removeClass('focus');
						self.focusIndex = $t.index();
					};
				},
				'mouseout.easyDropDown': function(){
					if(!self.keyboardMode){
						$(this).removeClass('focus');
					};
				}
			});

			self.$select.on({
				'focus.easyDropDown': function(){
					self.$container.addClass('focus');
					self.inFocus = true;
				},
				'blur.easyDropDown': function(){
					self.$container.removeClass('focus');
					self.inFocus = false;
				},
				'keydown.easyDropDown': function(e){
					if(self.inFocus){
						self.keyboardMode = true;
						var key = e.keyCode;

						if(key == 38 || key == 40 || key == 32){
							e.preventDefault();
							if(key == 38){
								self.focusIndex--
								self.focusIndex = self.focusIndex < 0 ? self.$items.length - 1 : self.focusIndex;
							} else if(key == 40){
								self.focusIndex++
								self.focusIndex = self.focusIndex > self.$items.length - 1 ? 0 : self.focusIndex;
							};
							if(!self.down){
								self.open();
							};
							self.$items.removeClass('focus').eq(self.focusIndex).addClass('focus');
							if(self.cutOff){
								self.scrollToView();
							};
							self.query = '';
						};
						if(self.down){
							if(key == 9 || key == 27){
								self.close();
							} else if(key == 13){
								e.preventDefault();
								self.select(self.focusIndex);
								self.close();
								return false;
							} else if(key == 8){
								e.preventDefault();
								self.query = self.query.slice(0,-1);
								self.search();
								clearTimeout(self.resetQuery);
								return false;
							} else if(key != 38 && key != 40){
								var letter = String.fromCharCode(key);
								self.query += letter;
								self.search();
								clearTimeout(self.resetQuery);
							};
						};
					};
				},
				'keyup.easyDropDown': function(){
					self.resetQuery = setTimeout(function(){
						self.query = '';
					},1200);
				}
			});
			
			self.$dropDown.on('scroll.easyDropDown',function(e){
				if(self.$dropDown[0].scrollTop >= self.$dropDown[0].scrollHeight - self.maxHeight){
					self.$container.addClass('bottom');
				} else {
					self.$container.removeClass('bottom');
				};
			});
			
			if(self.$form.length){
				self.$form.on('reset.easyDropDown', function(){
					var active = self.hasLabel ? self.label : self.options[0].title;
					self.$active.text(active);
				});
			};
		},
		
		unbindHandlers: function(){
			var self = this;
			
			self.$container
				.add(self.$select)
				.add(self.$items)
				.add(self.$form)
				.add(self.$dropDown)
				.off('.easyDropDown');
			$('body').off('.'+self.id);
		},
		
		open: function(){
			var self = this,
				scrollTop = window.scrollY || document.documentElement.scrollTop,
				scrollLeft = window.scrollX || document.documentElement.scrollLeft,
				scrollOffset = self.notInViewport(scrollTop);

			self.closeAll();
			self.getMaxHeight();
			self.$select.focus();
			window.scrollTo(scrollLeft, scrollTop+scrollOffset);
			self.$container.addClass('open');
			self.$scrollWrapper.css('height',self.maxHeight+'px');
			self.down = true;
		},
		
		close: function(){
			var self = this;
			self.$container.removeClass('open');
			self.$scrollWrapper.css('height','0px');
			self.focusIndex = self.selected.index;
			self.query = '';
			self.down = false;
		},
		
		closeAll: function(){
			var self = this,
				instances = Object.getPrototypeOf(self).instances;
			for(var key in instances){
				var instance = instances[key];
				instance.close();
			};
		},
	
		select: function(index){
			var self = this;
			
			if(typeof index === 'string'){
				index = self.$select.find('option[value='+index+']').index() - 1;
			};
			
			var	option = self.options[index],
				selectIndex = self.hasLabel ? index + 1 : index;
			self.$items.removeClass('active').eq(index).addClass('active');
			self.$active.text(option.title);
			self.$select
				.find('option')
				.removeAttr('selected')
				.eq(selectIndex)
				.prop('selected',true)
				.parent()
				.trigger('change');
				
			self.selected = {
				index: index,
				title: option.title
			};
			self.focusIndex = i;
			if(typeof self.onChange === 'function'){
				self.onChange.call(self.$select[0],{
					title: option.title, 
					value: option.value
				});
			};
		},
		
		search: function(){
			var self = this,
				lock = function(i){
					self.focusIndex = i;
					self.$items.removeClass('focus').eq(self.focusIndex).addClass('focus');
					self.scrollToView();	
				},
				getTitle = function(i){
					return self.options[i].title.toUpperCase();
				};
				
			for(i = 0; i < self.options.length; i++){
				var title = getTitle(i);
				if(title.indexOf(self.query) == 0){
					lock(i);
					return;
				};
			};
			
			for(i = 0; i < self.options.length; i++){
				var title = getTitle(i);
				if(title.indexOf(self.query) > -1){
					lock(i);
					break;
				};
			};
		},
		
		scrollToView: function(){
			var self = this;
			if(self.focusIndex >= self.cutOff){
				var $focusItem = self.$items.eq(self.focusIndex),
					scroll = ($focusItem.outerHeight() * (self.focusIndex + 1)) - self.maxHeight;
			
				self.$dropDown.scrollTop(scroll);
			};
		},
		
		notInViewport: function(scrollTop){
			var self = this,
				range = {
					min: scrollTop,
					max: scrollTop + (window.innerHeight || document.documentElement.clientHeight)
				},
				menuBottom = self.$dropDown.offset().top + self.maxHeight;
				
			if(menuBottom >= range.min && menuBottom <= range.max){
				return 0;
			} else {
				return (menuBottom - range.max) + 5;
			};
		},
		
		destroy: function(){
			var self = this;
			self.unbindHandlers();
			self.$select.unwrap().siblings().remove();
			self.$select.unwrap();
			delete Object.getPrototypeOf(self).instances[self.$select[0].id];
		},
		
		disable: function(){
			var self = this;
			self.disabled = true;
			self.$container.addClass('disabled');
			self.$select.attr('disabled',true);
			if(!self.down)self.close();
		},
		
		enable: function(){
			var self = this;
			self.disabled = false;
			self.$container.removeClass('disabled');
			self.$select.attr('disabled',false);
		}
	};
	
	var instantiate = function(domNode, settings){
			domNode.id = !domNode.id ? 'EasyDropDown'+rand() : domNode.id;
			var instance = new EasyDropDown();
			if(!instance.instances[domNode.id]){
				instance.instances[domNode.id] = instance;
				instance.init(domNode, settings);
			};
		},
		rand = function(){
			return ('00000'+(Math.random()*16777216<<0).toString(16)).substr(-6).toUpperCase();
		};
	
	$.fn.easyDropDown = function(){
		var args = arguments,
			dataReturn = [],
			eachReturn;
			
		eachReturn = this.each(function(){
			if(args && typeof args[0] === 'string'){
				var data = EasyDropDown.prototype.instances[this.id][args[0]](args[1], args[2]);
				if(data)dataReturn.push(data);
			} else {
				instantiate(this, args[0]);
			};
		});
		
		if(dataReturn.length){
			return dataReturn.length > 1 ? dataReturn : dataReturn[0];
		} else {
			return eachReturn;
		};
	};
	
	$(function(){
		if(typeof Object.getPrototypeOf !== 'function'){
			if(typeof 'test'.__proto__ === 'object'){
				Object.getPrototypeOf = function(object){
					return object.__proto__;
				};
			} else {
				Object.getPrototypeOf = function(object){
					return object.constructor.prototype;
				};
			};
		};
		
		$('select.dropdown').each(function(){
			var json = $(this).attr('data-settings');
				settings = json ? $.parseJSON(json) : {}; 
			instantiate(this, settings);
		});
	});
})(jQuery);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJqcXVlcnkuZWFzeWRyb3Bkb3duLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qXG4qIEVBU1lEUk9QRE9XTiAtIEEgRHJvcC1kb3duIEJ1aWxkZXIgZm9yIFN0eWxlYWJsZSBJbnB1dHMgYW5kIE1lbnVzXG4qIFZlcnNpb246IDIuMS40XG4qIExpY2Vuc2U6IENyZWF0aXZlIENvbW1vbnMgQXR0cmlidXRpb24gMy4wIFVucG9ydGVkIC0gQ0MgQlkgMy4wXG4qIGh0dHA6Ly9jcmVhdGl2ZWNvbW1vbnMub3JnL2xpY2Vuc2VzL2J5LzMuMC9cbiogVGhpcyBzb2Z0d2FyZSBtYXkgYmUgdXNlZCBmcmVlbHkgb24gY29tbWVyY2lhbCBhbmQgbm9uLWNvbW1lcmNpYWwgcHJvamVjdHMgd2l0aCBhdHRyaWJ1dGlvbiB0byB0aGUgYXV0aG9yL2NvcHlyaWdodCBob2xkZXIuXG4qIEF1dGhvcjogUGF0cmljayBLdW5rYVxuKiBDb3B5cmlnaHQgMjAxMyBQYXRyaWNrIEt1bmthLCBBbGwgUmlnaHRzIFJlc2VydmVkXG4qL1xuXG5cbihmdW5jdGlvbigkKXtcblx0XG5cdGZ1bmN0aW9uIEVhc3lEcm9wRG93bigpe1xuXHRcdHRoaXMuaXNGaWVsZCA9IHRydWUsXG5cdFx0dGhpcy5kb3duID0gZmFsc2UsXG5cdFx0dGhpcy5pbkZvY3VzID0gZmFsc2UsXG5cdFx0dGhpcy5kaXNhYmxlZCA9IGZhbHNlLFxuXHRcdHRoaXMuY3V0T2ZmID0gZmFsc2UsXG5cdFx0dGhpcy5oYXNMYWJlbCA9IGZhbHNlLFxuXHRcdHRoaXMua2V5Ym9hcmRNb2RlID0gZmFsc2UsXG5cdFx0dGhpcy5uYXRpdmVUb3VjaCA9IHRydWUsXG5cdFx0dGhpcy53cmFwcGVyQ2xhc3MgPSAnZHJvcGRvd24nLFxuXHRcdHRoaXMub25DaGFuZ2UgPSBudWxsO1xuXHR9O1xuXHRcblx0RWFzeURyb3BEb3duLnByb3RvdHlwZSA9IHtcblx0XHRjb25zdHJ1Y3RvcjogRWFzeURyb3BEb3duLFxuXHRcdGluc3RhbmNlczoge30sXG5cdFx0aW5pdDogZnVuY3Rpb24oZG9tTm9kZSwgc2V0dGluZ3Mpe1xuXHRcdFx0dmFyXHRzZWxmID0gdGhpcztcblx0XHRcdFxuXHRcdFx0JC5leHRlbmQoc2VsZiwgc2V0dGluZ3MpO1xuXHRcdFx0c2VsZi4kc2VsZWN0ID0gJChkb21Ob2RlKTtcblx0XHRcdHNlbGYuaWQgPSBkb21Ob2RlLmlkO1xuXHRcdFx0c2VsZi5vcHRpb25zID0gW107XG5cdFx0XHRzZWxmLiRvcHRpb25zID0gc2VsZi4kc2VsZWN0LmZpbmQoJ29wdGlvbicpO1xuXHRcdFx0c2VsZi5pc1RvdWNoID0gJ29udG91Y2hlbmQnIGluIGRvY3VtZW50O1xuXHRcdFx0c2VsZi4kc2VsZWN0LnJlbW92ZUNsYXNzKHNlbGYud3JhcHBlckNsYXNzKycgZHJvcGRvd24nKTtcblx0XHRcdGlmKHNlbGYuJHNlbGVjdC5pcygnOmRpc2FibGVkJykpe1xuXHRcdFx0XHRzZWxmLmRpc2FibGVkID0gdHJ1ZTtcblx0XHRcdH07XG5cdFx0XHRpZihzZWxmLiRvcHRpb25zLmxlbmd0aCl7XG5cdFx0XHRcdHNlbGYuJG9wdGlvbnMuZWFjaChmdW5jdGlvbihpKXtcblx0XHRcdFx0XHR2YXIgJG9wdGlvbiA9ICQodGhpcyk7XG5cdFx0XHRcdFx0aWYoJG9wdGlvbi5pcygnOnNlbGVjdGVkJykpe1xuXHRcdFx0XHRcdFx0c2VsZi5zZWxlY3RlZCA9IHtcblx0XHRcdFx0XHRcdFx0aW5kZXg6IGksXG5cdFx0XHRcdFx0XHRcdHRpdGxlOiAkb3B0aW9uLnRleHQoKVxuXHRcdFx0XHRcdFx0fVxuXHRcdFx0XHRcdFx0c2VsZi5mb2N1c0luZGV4ID0gaTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdGlmKCRvcHRpb24uaGFzQ2xhc3MoJ2xhYmVsJykgJiYgaSA9PSAwKXtcblx0XHRcdFx0XHRcdHNlbGYuaGFzTGFiZWwgPSB0cnVlO1xuXHRcdFx0XHRcdFx0c2VsZi5sYWJlbCA9ICRvcHRpb24udGV4dCgpO1xuXHRcdFx0XHRcdFx0JG9wdGlvbi5hdHRyKCd2YWx1ZScsJycpO1xuXHRcdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0XHRzZWxmLm9wdGlvbnMucHVzaCh7XG5cdFx0XHRcdFx0XHRcdGRvbU5vZGU6ICRvcHRpb25bMF0sXG5cdFx0XHRcdFx0XHRcdHRpdGxlOiAkb3B0aW9uLnRleHQoKSxcblx0XHRcdFx0XHRcdFx0dmFsdWU6ICRvcHRpb24udmFsKCksXG5cdFx0XHRcdFx0XHRcdHNlbGVjdGVkOiAkb3B0aW9uLmlzKCc6c2VsZWN0ZWQnKVxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0fSk7XG5cdFx0XHRcdGlmKCFzZWxmLnNlbGVjdGVkKXtcblx0XHRcdFx0XHRzZWxmLnNlbGVjdGVkID0ge1xuXHRcdFx0XHRcdFx0aW5kZXg6IDAsXG5cdFx0XHRcdFx0XHR0aXRsZTogc2VsZi4kb3B0aW9ucy5lcSgwKS50ZXh0KClcblx0XHRcdFx0XHR9XG5cdFx0XHRcdFx0c2VsZi5mb2N1c0luZGV4ID0gMDtcblx0XHRcdFx0fTtcblx0XHRcdFx0c2VsZi5yZW5kZXIoKTtcblx0XHRcdH07XG5cdFx0fSxcblx0XG5cdFx0cmVuZGVyOiBmdW5jdGlvbigpe1xuXHRcdFx0dmFyXHRzZWxmID0gdGhpcyxcblx0XHRcdFx0dG91Y2hDbGFzcyA9IHNlbGYuaXNUb3VjaCAmJiBzZWxmLm5hdGl2ZVRvdWNoID8gJyB0b3VjaCcgOiAnJyxcblx0XHRcdFx0ZGlzYWJsZWRDbGFzcyA9IHNlbGYuZGlzYWJsZWQgPyAnIGRpc2FibGVkJyA6ICcnO1xuXHRcdFx0XG5cdFx0XHRzZWxmLiRjb250YWluZXIgPSBzZWxmLiRzZWxlY3Qud3JhcCgnPGRpdiBjbGFzcz1cIicrc2VsZi53cmFwcGVyQ2xhc3MrdG91Y2hDbGFzcytkaXNhYmxlZENsYXNzKydcIj48c3BhbiBjbGFzcz1cIm9sZFwiLz48L2Rpdj4nKS5wYXJlbnQoKS5wYXJlbnQoKTtcblx0XHRcdHNlbGYuJGFjdGl2ZSA9ICQoJzxzcGFuIGNsYXNzPVwic2VsZWN0ZWRcIj4nK3NlbGYuc2VsZWN0ZWQudGl0bGUrJzwvc3Bhbj4nKS5hcHBlbmRUbyhzZWxmLiRjb250YWluZXIpO1xuXHRcdFx0c2VsZi4kY2FyYXQgPSAkKCc8c3BhbiBjbGFzcz1cImNhcmF0XCIvPicpLmFwcGVuZFRvKHNlbGYuJGNvbnRhaW5lcik7XG5cdFx0XHRzZWxmLiRzY3JvbGxXcmFwcGVyID0gJCgnPGRpdj48dWwvPjwvZGl2PicpLmFwcGVuZFRvKHNlbGYuJGNvbnRhaW5lcik7XG5cdFx0XHRzZWxmLiRkcm9wRG93biA9IHNlbGYuJHNjcm9sbFdyYXBwZXIuZmluZCgndWwnKTtcblx0XHRcdHNlbGYuJGZvcm0gPSBzZWxmLiRjb250YWluZXIuY2xvc2VzdCgnZm9ybScpO1xuXHRcdFx0JC5lYWNoKHNlbGYub3B0aW9ucywgZnVuY3Rpb24oKXtcblx0XHRcdFx0dmFyXHRvcHRpb24gPSB0aGlzLFxuXHRcdFx0XHRcdGFjdGl2ZSA9IG9wdGlvbi5zZWxlY3RlZCA/ICcgY2xhc3M9XCJhY3RpdmVcIic6Jyc7XG5cdFx0XHRcdHNlbGYuJGRyb3BEb3duLmFwcGVuZCgnPGxpJythY3RpdmUrJz4nK29wdGlvbi50aXRsZSsnPC9saT4nKTtcblx0XHRcdH0pO1xuXHRcdFx0c2VsZi4kaXRlbXMgPSBzZWxmLiRkcm9wRG93bi5maW5kKCdsaScpO1xuXHRcdFx0XG5cdFx0XHRpZihzZWxmLmN1dE9mZiAmJiBzZWxmLiRpdGVtcy5sZW5ndGggPiBzZWxmLmN1dE9mZilzZWxmLiRjb250YWluZXIuYWRkQ2xhc3MoJ3Njcm9sbGFibGUnKTtcblx0XHRcdFxuXHRcdFx0c2VsZi5nZXRNYXhIZWlnaHQoKTtcblx0XG5cdFx0XHRpZihzZWxmLmlzVG91Y2ggJiYgc2VsZi5uYXRpdmVUb3VjaCl7XG5cdFx0XHRcdHNlbGYuYmluZFRvdWNoSGFuZGxlcnMoKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHNlbGYuYmluZEhhbmRsZXJzKCk7XG5cdFx0XHR9O1xuXHRcdH0sXG5cdFx0XG5cdFx0Z2V0TWF4SGVpZ2h0OiBmdW5jdGlvbigpe1xuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0XG5cdFx0XHRzZWxmLm1heEhlaWdodCA9IDA7XG5cdFx0XHRcblx0XHRcdGZvcihpID0gMDsgaSA8IHNlbGYuJGl0ZW1zLmxlbmd0aDsgaSsrKXtcblx0XHRcdFx0dmFyICRpdGVtID0gc2VsZi4kaXRlbXMuZXEoaSk7XG5cdFx0XHRcdHNlbGYubWF4SGVpZ2h0ICs9ICRpdGVtLm91dGVySGVpZ2h0KCk7XG5cdFx0XHRcdGlmKHNlbGYuY3V0T2ZmID09IGkrMSl7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH07XG5cdFx0XHR9O1xuXHRcdH0sXG5cdFx0XG5cdFx0YmluZFRvdWNoSGFuZGxlcnM6IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXJcdHNlbGYgPSB0aGlzO1xuXHRcdFx0c2VsZi4kY29udGFpbmVyLm9uKCdjbGljay5lYXN5RHJvcERvd24nLGZ1bmN0aW9uKCl7XG5cdFx0XHRcdHNlbGYuJHNlbGVjdC5mb2N1cygpO1xuXHRcdFx0fSk7XG5cdFx0XHRzZWxmLiRzZWxlY3Qub24oe1xuXHRcdFx0XHRjaGFuZ2U6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0dmFyXHQkc2VsZWN0ZWQgPSAkKHRoaXMpLmZpbmQoJ29wdGlvbjpzZWxlY3RlZCcpLFxuXHRcdFx0XHRcdFx0dGl0bGUgPSAkc2VsZWN0ZWQudGV4dCgpLFxuXHRcdFx0XHRcdFx0dmFsdWUgPSAkc2VsZWN0ZWQudmFsKCk7XG5cdFx0XHRcdFx0XHRcblx0XHRcdFx0XHRzZWxmLiRhY3RpdmUudGV4dCh0aXRsZSk7XG5cdFx0XHRcdFx0aWYodHlwZW9mIHNlbGYub25DaGFuZ2UgPT09ICdmdW5jdGlvbicpe1xuXHRcdFx0XHRcdFx0c2VsZi5vbkNoYW5nZS5jYWxsKHNlbGYuJHNlbGVjdFswXSx7XG5cdFx0XHRcdFx0XHRcdHRpdGxlOiB0aXRsZSwgXG5cdFx0XHRcdFx0XHRcdHZhbHVlOiB2YWx1ZVxuXHRcdFx0XHRcdFx0fSk7XG5cdFx0XHRcdFx0fTtcblx0XHRcdFx0fSxcblx0XHRcdFx0Zm9jdXM6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0c2VsZi4kY29udGFpbmVyLmFkZENsYXNzKCdmb2N1cycpO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHRibHVyOiBmdW5jdGlvbigpe1xuXHRcdFx0XHRcdHNlbGYuJGNvbnRhaW5lci5yZW1vdmVDbGFzcygnZm9jdXMnKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0fSxcblx0XG5cdFx0YmluZEhhbmRsZXJzOiBmdW5jdGlvbigpe1xuXHRcdFx0dmFyXHRzZWxmID0gdGhpcztcblx0XHRcdHNlbGYucXVlcnkgPSAnJztcblx0XHRcdHNlbGYuJGNvbnRhaW5lci5vbih7XG5cdFx0XHRcdCdjbGljay5lYXN5RHJvcERvd24nOiBmdW5jdGlvbigpe1xuXHRcdFx0XHRcdGlmKCFzZWxmLmRvd24gJiYgIXNlbGYuZGlzYWJsZWQpe1xuXHRcdFx0XHRcdFx0c2VsZi5vcGVuKCk7XG5cdFx0XHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0XHRcdHNlbGYuY2xvc2UoKTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9LFxuXHRcdFx0XHQnbW91c2Vtb3ZlLmVhc3lEcm9wRG93bic6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0aWYoc2VsZi5rZXlib2FyZE1vZGUpe1xuXHRcdFx0XHRcdFx0c2VsZi5rZXlib2FyZE1vZGUgPSBmYWxzZTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0JCgnYm9keScpLm9uKCdjbGljay5lYXN5RHJvcERvd24uJytzZWxmLmlkLGZ1bmN0aW9uKGUpe1xuXHRcdFx0XHR2YXIgJHRhcmdldCA9ICQoZS50YXJnZXQpLFxuXHRcdFx0XHRcdGNsYXNzTmFtZXMgPSBzZWxmLndyYXBwZXJDbGFzcy5zcGxpdCgnICcpLmpvaW4oJy4nKTtcblxuXHRcdFx0XHRpZighJHRhcmdldC5jbG9zZXN0KCcuJytjbGFzc05hbWVzKS5sZW5ndGggJiYgc2VsZi5kb3duKXtcblx0XHRcdFx0XHRzZWxmLmNsb3NlKCk7XG5cdFx0XHRcdH07XG5cdFx0XHR9KTtcblxuXHRcdFx0c2VsZi4kaXRlbXMub24oe1xuXHRcdFx0XHQnY2xpY2suZWFzeURyb3BEb3duJzogZnVuY3Rpb24oKXtcblx0XHRcdFx0XHR2YXIgaW5kZXggPSAkKHRoaXMpLmluZGV4KCk7XG5cdFx0XHRcdFx0c2VsZi5zZWxlY3QoaW5kZXgpO1xuXHRcdFx0XHRcdHNlbGYuJHNlbGVjdC5mb2N1cygpO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHQnbW91c2VvdmVyLmVhc3lEcm9wRG93bic6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0aWYoIXNlbGYua2V5Ym9hcmRNb2RlKXtcblx0XHRcdFx0XHRcdHZhciAkdCA9ICQodGhpcyk7XG5cdFx0XHRcdFx0XHQkdC5hZGRDbGFzcygnZm9jdXMnKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKCdmb2N1cycpO1xuXHRcdFx0XHRcdFx0c2VsZi5mb2N1c0luZGV4ID0gJHQuaW5kZXgoKTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9LFxuXHRcdFx0XHQnbW91c2VvdXQuZWFzeURyb3BEb3duJzogZnVuY3Rpb24oKXtcblx0XHRcdFx0XHRpZighc2VsZi5rZXlib2FyZE1vZGUpe1xuXHRcdFx0XHRcdFx0JCh0aGlzKS5yZW1vdmVDbGFzcygnZm9jdXMnKTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9XG5cdFx0XHR9KTtcblxuXHRcdFx0c2VsZi4kc2VsZWN0Lm9uKHtcblx0XHRcdFx0J2ZvY3VzLmVhc3lEcm9wRG93bic6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0c2VsZi4kY29udGFpbmVyLmFkZENsYXNzKCdmb2N1cycpO1xuXHRcdFx0XHRcdHNlbGYuaW5Gb2N1cyA9IHRydWU7XG5cdFx0XHRcdH0sXG5cdFx0XHRcdCdibHVyLmVhc3lEcm9wRG93bic6IGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0c2VsZi4kY29udGFpbmVyLnJlbW92ZUNsYXNzKCdmb2N1cycpO1xuXHRcdFx0XHRcdHNlbGYuaW5Gb2N1cyA9IGZhbHNlO1xuXHRcdFx0XHR9LFxuXHRcdFx0XHQna2V5ZG93bi5lYXN5RHJvcERvd24nOiBmdW5jdGlvbihlKXtcblx0XHRcdFx0XHRpZihzZWxmLmluRm9jdXMpe1xuXHRcdFx0XHRcdFx0c2VsZi5rZXlib2FyZE1vZGUgPSB0cnVlO1xuXHRcdFx0XHRcdFx0dmFyIGtleSA9IGUua2V5Q29kZTtcblxuXHRcdFx0XHRcdFx0aWYoa2V5ID09IDM4IHx8IGtleSA9PSA0MCB8fCBrZXkgPT0gMzIpe1xuXHRcdFx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHRcdFx0XHRcdGlmKGtleSA9PSAzOCl7XG5cdFx0XHRcdFx0XHRcdFx0c2VsZi5mb2N1c0luZGV4LS1cblx0XHRcdFx0XHRcdFx0XHRzZWxmLmZvY3VzSW5kZXggPSBzZWxmLmZvY3VzSW5kZXggPCAwID8gc2VsZi4kaXRlbXMubGVuZ3RoIC0gMSA6IHNlbGYuZm9jdXNJbmRleDtcblx0XHRcdFx0XHRcdFx0fSBlbHNlIGlmKGtleSA9PSA0MCl7XG5cdFx0XHRcdFx0XHRcdFx0c2VsZi5mb2N1c0luZGV4Kytcblx0XHRcdFx0XHRcdFx0XHRzZWxmLmZvY3VzSW5kZXggPSBzZWxmLmZvY3VzSW5kZXggPiBzZWxmLiRpdGVtcy5sZW5ndGggLSAxID8gMCA6IHNlbGYuZm9jdXNJbmRleDtcblx0XHRcdFx0XHRcdFx0fTtcblx0XHRcdFx0XHRcdFx0aWYoIXNlbGYuZG93bil7XG5cdFx0XHRcdFx0XHRcdFx0c2VsZi5vcGVuKCk7XG5cdFx0XHRcdFx0XHRcdH07XG5cdFx0XHRcdFx0XHRcdHNlbGYuJGl0ZW1zLnJlbW92ZUNsYXNzKCdmb2N1cycpLmVxKHNlbGYuZm9jdXNJbmRleCkuYWRkQ2xhc3MoJ2ZvY3VzJyk7XG5cdFx0XHRcdFx0XHRcdGlmKHNlbGYuY3V0T2ZmKXtcblx0XHRcdFx0XHRcdFx0XHRzZWxmLnNjcm9sbFRvVmlldygpO1xuXHRcdFx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdFx0XHRzZWxmLnF1ZXJ5ID0gJyc7XG5cdFx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdFx0aWYoc2VsZi5kb3duKXtcblx0XHRcdFx0XHRcdFx0aWYoa2V5ID09IDkgfHwga2V5ID09IDI3KXtcblx0XHRcdFx0XHRcdFx0XHRzZWxmLmNsb3NlKCk7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSBpZihrZXkgPT0gMTMpe1xuXHRcdFx0XHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0XHRcdFx0XHRzZWxmLnNlbGVjdChzZWxmLmZvY3VzSW5kZXgpO1xuXHRcdFx0XHRcdFx0XHRcdHNlbGYuY2xvc2UoKTtcblx0XHRcdFx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdFx0XHRcdH0gZWxzZSBpZihrZXkgPT0gOCl7XG5cdFx0XHRcdFx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdFx0XHRcdFx0XHRcdHNlbGYucXVlcnkgPSBzZWxmLnF1ZXJ5LnNsaWNlKDAsLTEpO1xuXHRcdFx0XHRcdFx0XHRcdHNlbGYuc2VhcmNoKCk7XG5cdFx0XHRcdFx0XHRcdFx0Y2xlYXJUaW1lb3V0KHNlbGYucmVzZXRRdWVyeSk7XG5cdFx0XHRcdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRcdFx0XHR9IGVsc2UgaWYoa2V5ICE9IDM4ICYmIGtleSAhPSA0MCl7XG5cdFx0XHRcdFx0XHRcdFx0dmFyIGxldHRlciA9IFN0cmluZy5mcm9tQ2hhckNvZGUoa2V5KTtcblx0XHRcdFx0XHRcdFx0XHRzZWxmLnF1ZXJ5ICs9IGxldHRlcjtcblx0XHRcdFx0XHRcdFx0XHRzZWxmLnNlYXJjaCgpO1xuXHRcdFx0XHRcdFx0XHRcdGNsZWFyVGltZW91dChzZWxmLnJlc2V0UXVlcnkpO1xuXHRcdFx0XHRcdFx0XHR9O1xuXHRcdFx0XHRcdFx0fTtcblx0XHRcdFx0XHR9O1xuXHRcdFx0XHR9LFxuXHRcdFx0XHQna2V5dXAuZWFzeURyb3BEb3duJzogZnVuY3Rpb24oKXtcblx0XHRcdFx0XHRzZWxmLnJlc2V0UXVlcnkgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uKCl7XG5cdFx0XHRcdFx0XHRzZWxmLnF1ZXJ5ID0gJyc7XG5cdFx0XHRcdFx0fSwxMjAwKTtcblx0XHRcdFx0fVxuXHRcdFx0fSk7XG5cdFx0XHRcblx0XHRcdHNlbGYuJGRyb3BEb3duLm9uKCdzY3JvbGwuZWFzeURyb3BEb3duJyxmdW5jdGlvbihlKXtcblx0XHRcdFx0aWYoc2VsZi4kZHJvcERvd25bMF0uc2Nyb2xsVG9wID49IHNlbGYuJGRyb3BEb3duWzBdLnNjcm9sbEhlaWdodCAtIHNlbGYubWF4SGVpZ2h0KXtcblx0XHRcdFx0XHRzZWxmLiRjb250YWluZXIuYWRkQ2xhc3MoJ2JvdHRvbScpO1xuXHRcdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRcdHNlbGYuJGNvbnRhaW5lci5yZW1vdmVDbGFzcygnYm90dG9tJyk7XG5cdFx0XHRcdH07XG5cdFx0XHR9KTtcblx0XHRcdFxuXHRcdFx0aWYoc2VsZi4kZm9ybS5sZW5ndGgpe1xuXHRcdFx0XHRzZWxmLiRmb3JtLm9uKCdyZXNldC5lYXN5RHJvcERvd24nLCBmdW5jdGlvbigpe1xuXHRcdFx0XHRcdHZhciBhY3RpdmUgPSBzZWxmLmhhc0xhYmVsID8gc2VsZi5sYWJlbCA6IHNlbGYub3B0aW9uc1swXS50aXRsZTtcblx0XHRcdFx0XHRzZWxmLiRhY3RpdmUudGV4dChhY3RpdmUpO1xuXHRcdFx0XHR9KTtcblx0XHRcdH07XG5cdFx0fSxcblx0XHRcblx0XHR1bmJpbmRIYW5kbGVyczogZnVuY3Rpb24oKXtcblx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdFxuXHRcdFx0c2VsZi4kY29udGFpbmVyXG5cdFx0XHRcdC5hZGQoc2VsZi4kc2VsZWN0KVxuXHRcdFx0XHQuYWRkKHNlbGYuJGl0ZW1zKVxuXHRcdFx0XHQuYWRkKHNlbGYuJGZvcm0pXG5cdFx0XHRcdC5hZGQoc2VsZi4kZHJvcERvd24pXG5cdFx0XHRcdC5vZmYoJy5lYXN5RHJvcERvd24nKTtcblx0XHRcdCQoJ2JvZHknKS5vZmYoJy4nK3NlbGYuaWQpO1xuXHRcdH0sXG5cdFx0XG5cdFx0b3BlbjogZnVuY3Rpb24oKXtcblx0XHRcdHZhciBzZWxmID0gdGhpcyxcblx0XHRcdFx0c2Nyb2xsVG9wID0gd2luZG93LnNjcm9sbFkgfHwgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbFRvcCxcblx0XHRcdFx0c2Nyb2xsTGVmdCA9IHdpbmRvdy5zY3JvbGxYIHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5zY3JvbGxMZWZ0LFxuXHRcdFx0XHRzY3JvbGxPZmZzZXQgPSBzZWxmLm5vdEluVmlld3BvcnQoc2Nyb2xsVG9wKTtcblxuXHRcdFx0c2VsZi5jbG9zZUFsbCgpO1xuXHRcdFx0c2VsZi5nZXRNYXhIZWlnaHQoKTtcblx0XHRcdHNlbGYuJHNlbGVjdC5mb2N1cygpO1xuXHRcdFx0d2luZG93LnNjcm9sbFRvKHNjcm9sbExlZnQsIHNjcm9sbFRvcCtzY3JvbGxPZmZzZXQpO1xuXHRcdFx0c2VsZi4kY29udGFpbmVyLmFkZENsYXNzKCdvcGVuJyk7XG5cdFx0XHRzZWxmLiRzY3JvbGxXcmFwcGVyLmNzcygnaGVpZ2h0JyxzZWxmLm1heEhlaWdodCsncHgnKTtcblx0XHRcdHNlbGYuZG93biA9IHRydWU7XG5cdFx0fSxcblx0XHRcblx0XHRjbG9zZTogZnVuY3Rpb24oKXtcblx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdHNlbGYuJGNvbnRhaW5lci5yZW1vdmVDbGFzcygnb3BlbicpO1xuXHRcdFx0c2VsZi4kc2Nyb2xsV3JhcHBlci5jc3MoJ2hlaWdodCcsJzBweCcpO1xuXHRcdFx0c2VsZi5mb2N1c0luZGV4ID0gc2VsZi5zZWxlY3RlZC5pbmRleDtcblx0XHRcdHNlbGYucXVlcnkgPSAnJztcblx0XHRcdHNlbGYuZG93biA9IGZhbHNlO1xuXHRcdH0sXG5cdFx0XG5cdFx0Y2xvc2VBbGw6IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgc2VsZiA9IHRoaXMsXG5cdFx0XHRcdGluc3RhbmNlcyA9IE9iamVjdC5nZXRQcm90b3R5cGVPZihzZWxmKS5pbnN0YW5jZXM7XG5cdFx0XHRmb3IodmFyIGtleSBpbiBpbnN0YW5jZXMpe1xuXHRcdFx0XHR2YXIgaW5zdGFuY2UgPSBpbnN0YW5jZXNba2V5XTtcblx0XHRcdFx0aW5zdGFuY2UuY2xvc2UoKTtcblx0XHRcdH07XG5cdFx0fSxcblx0XG5cdFx0c2VsZWN0OiBmdW5jdGlvbihpbmRleCl7XG5cdFx0XHR2YXIgc2VsZiA9IHRoaXM7XG5cdFx0XHRcblx0XHRcdGlmKHR5cGVvZiBpbmRleCA9PT0gJ3N0cmluZycpe1xuXHRcdFx0XHRpbmRleCA9IHNlbGYuJHNlbGVjdC5maW5kKCdvcHRpb25bdmFsdWU9JytpbmRleCsnXScpLmluZGV4KCkgLSAxO1xuXHRcdFx0fTtcblx0XHRcdFxuXHRcdFx0dmFyXHRvcHRpb24gPSBzZWxmLm9wdGlvbnNbaW5kZXhdLFxuXHRcdFx0XHRzZWxlY3RJbmRleCA9IHNlbGYuaGFzTGFiZWwgPyBpbmRleCArIDEgOiBpbmRleDtcblx0XHRcdHNlbGYuJGl0ZW1zLnJlbW92ZUNsYXNzKCdhY3RpdmUnKS5lcShpbmRleCkuYWRkQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdFx0c2VsZi4kYWN0aXZlLnRleHQob3B0aW9uLnRpdGxlKTtcblx0XHRcdHNlbGYuJHNlbGVjdFxuXHRcdFx0XHQuZmluZCgnb3B0aW9uJylcblx0XHRcdFx0LnJlbW92ZUF0dHIoJ3NlbGVjdGVkJylcblx0XHRcdFx0LmVxKHNlbGVjdEluZGV4KVxuXHRcdFx0XHQucHJvcCgnc2VsZWN0ZWQnLHRydWUpXG5cdFx0XHRcdC5wYXJlbnQoKVxuXHRcdFx0XHQudHJpZ2dlcignY2hhbmdlJyk7XG5cdFx0XHRcdFxuXHRcdFx0c2VsZi5zZWxlY3RlZCA9IHtcblx0XHRcdFx0aW5kZXg6IGluZGV4LFxuXHRcdFx0XHR0aXRsZTogb3B0aW9uLnRpdGxlXG5cdFx0XHR9O1xuXHRcdFx0c2VsZi5mb2N1c0luZGV4ID0gaTtcblx0XHRcdGlmKHR5cGVvZiBzZWxmLm9uQ2hhbmdlID09PSAnZnVuY3Rpb24nKXtcblx0XHRcdFx0c2VsZi5vbkNoYW5nZS5jYWxsKHNlbGYuJHNlbGVjdFswXSx7XG5cdFx0XHRcdFx0dGl0bGU6IG9wdGlvbi50aXRsZSwgXG5cdFx0XHRcdFx0dmFsdWU6IG9wdGlvbi52YWx1ZVxuXHRcdFx0XHR9KTtcblx0XHRcdH07XG5cdFx0fSxcblx0XHRcblx0XHRzZWFyY2g6IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgc2VsZiA9IHRoaXMsXG5cdFx0XHRcdGxvY2sgPSBmdW5jdGlvbihpKXtcblx0XHRcdFx0XHRzZWxmLmZvY3VzSW5kZXggPSBpO1xuXHRcdFx0XHRcdHNlbGYuJGl0ZW1zLnJlbW92ZUNsYXNzKCdmb2N1cycpLmVxKHNlbGYuZm9jdXNJbmRleCkuYWRkQ2xhc3MoJ2ZvY3VzJyk7XG5cdFx0XHRcdFx0c2VsZi5zY3JvbGxUb1ZpZXcoKTtcdFxuXHRcdFx0XHR9LFxuXHRcdFx0XHRnZXRUaXRsZSA9IGZ1bmN0aW9uKGkpe1xuXHRcdFx0XHRcdHJldHVybiBzZWxmLm9wdGlvbnNbaV0udGl0bGUudG9VcHBlckNhc2UoKTtcblx0XHRcdFx0fTtcblx0XHRcdFx0XG5cdFx0XHRmb3IoaSA9IDA7IGkgPCBzZWxmLm9wdGlvbnMubGVuZ3RoOyBpKyspe1xuXHRcdFx0XHR2YXIgdGl0bGUgPSBnZXRUaXRsZShpKTtcblx0XHRcdFx0aWYodGl0bGUuaW5kZXhPZihzZWxmLnF1ZXJ5KSA9PSAwKXtcblx0XHRcdFx0XHRsb2NrKGkpO1xuXHRcdFx0XHRcdHJldHVybjtcblx0XHRcdFx0fTtcblx0XHRcdH07XG5cdFx0XHRcblx0XHRcdGZvcihpID0gMDsgaSA8IHNlbGYub3B0aW9ucy5sZW5ndGg7IGkrKyl7XG5cdFx0XHRcdHZhciB0aXRsZSA9IGdldFRpdGxlKGkpO1xuXHRcdFx0XHRpZih0aXRsZS5pbmRleE9mKHNlbGYucXVlcnkpID4gLTEpe1xuXHRcdFx0XHRcdGxvY2soaSk7XG5cdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdH07XG5cdFx0XHR9O1xuXHRcdH0sXG5cdFx0XG5cdFx0c2Nyb2xsVG9WaWV3OiBmdW5jdGlvbigpe1xuXHRcdFx0dmFyIHNlbGYgPSB0aGlzO1xuXHRcdFx0aWYoc2VsZi5mb2N1c0luZGV4ID49IHNlbGYuY3V0T2ZmKXtcblx0XHRcdFx0dmFyICRmb2N1c0l0ZW0gPSBzZWxmLiRpdGVtcy5lcShzZWxmLmZvY3VzSW5kZXgpLFxuXHRcdFx0XHRcdHNjcm9sbCA9ICgkZm9jdXNJdGVtLm91dGVySGVpZ2h0KCkgKiAoc2VsZi5mb2N1c0luZGV4ICsgMSkpIC0gc2VsZi5tYXhIZWlnaHQ7XG5cdFx0XHRcblx0XHRcdFx0c2VsZi4kZHJvcERvd24uc2Nyb2xsVG9wKHNjcm9sbCk7XG5cdFx0XHR9O1xuXHRcdH0sXG5cdFx0XG5cdFx0bm90SW5WaWV3cG9ydDogZnVuY3Rpb24oc2Nyb2xsVG9wKXtcblx0XHRcdHZhciBzZWxmID0gdGhpcyxcblx0XHRcdFx0cmFuZ2UgPSB7XG5cdFx0XHRcdFx0bWluOiBzY3JvbGxUb3AsXG5cdFx0XHRcdFx0bWF4OiBzY3JvbGxUb3AgKyAod2luZG93LmlubmVySGVpZ2h0IHx8IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGllbnRIZWlnaHQpXG5cdFx0XHRcdH0sXG5cdFx0XHRcdG1lbnVCb3R0b20gPSBzZWxmLiRkcm9wRG93bi5vZmZzZXQoKS50b3AgKyBzZWxmLm1heEhlaWdodDtcblx0XHRcdFx0XG5cdFx0XHRpZihtZW51Qm90dG9tID49IHJhbmdlLm1pbiAmJiBtZW51Qm90dG9tIDw9IHJhbmdlLm1heCl7XG5cdFx0XHRcdHJldHVybiAwO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0cmV0dXJuIChtZW51Qm90dG9tIC0gcmFuZ2UubWF4KSArIDU7XG5cdFx0XHR9O1xuXHRcdH0sXG5cdFx0XG5cdFx0ZGVzdHJveTogZnVuY3Rpb24oKXtcblx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdHNlbGYudW5iaW5kSGFuZGxlcnMoKTtcblx0XHRcdHNlbGYuJHNlbGVjdC51bndyYXAoKS5zaWJsaW5ncygpLnJlbW92ZSgpO1xuXHRcdFx0c2VsZi4kc2VsZWN0LnVud3JhcCgpO1xuXHRcdFx0ZGVsZXRlIE9iamVjdC5nZXRQcm90b3R5cGVPZihzZWxmKS5pbnN0YW5jZXNbc2VsZi4kc2VsZWN0WzBdLmlkXTtcblx0XHR9LFxuXHRcdFxuXHRcdGRpc2FibGU6IGZ1bmN0aW9uKCl7XG5cdFx0XHR2YXIgc2VsZiA9IHRoaXM7XG5cdFx0XHRzZWxmLmRpc2FibGVkID0gdHJ1ZTtcblx0XHRcdHNlbGYuJGNvbnRhaW5lci5hZGRDbGFzcygnZGlzYWJsZWQnKTtcblx0XHRcdHNlbGYuJHNlbGVjdC5hdHRyKCdkaXNhYmxlZCcsdHJ1ZSk7XG5cdFx0XHRpZighc2VsZi5kb3duKXNlbGYuY2xvc2UoKTtcblx0XHR9LFxuXHRcdFxuXHRcdGVuYWJsZTogZnVuY3Rpb24oKXtcblx0XHRcdHZhciBzZWxmID0gdGhpcztcblx0XHRcdHNlbGYuZGlzYWJsZWQgPSBmYWxzZTtcblx0XHRcdHNlbGYuJGNvbnRhaW5lci5yZW1vdmVDbGFzcygnZGlzYWJsZWQnKTtcblx0XHRcdHNlbGYuJHNlbGVjdC5hdHRyKCdkaXNhYmxlZCcsZmFsc2UpO1xuXHRcdH1cblx0fTtcblx0XG5cdHZhciBpbnN0YW50aWF0ZSA9IGZ1bmN0aW9uKGRvbU5vZGUsIHNldHRpbmdzKXtcblx0XHRcdGRvbU5vZGUuaWQgPSAhZG9tTm9kZS5pZCA/ICdFYXN5RHJvcERvd24nK3JhbmQoKSA6IGRvbU5vZGUuaWQ7XG5cdFx0XHR2YXIgaW5zdGFuY2UgPSBuZXcgRWFzeURyb3BEb3duKCk7XG5cdFx0XHRpZighaW5zdGFuY2UuaW5zdGFuY2VzW2RvbU5vZGUuaWRdKXtcblx0XHRcdFx0aW5zdGFuY2UuaW5zdGFuY2VzW2RvbU5vZGUuaWRdID0gaW5zdGFuY2U7XG5cdFx0XHRcdGluc3RhbmNlLmluaXQoZG9tTm9kZSwgc2V0dGluZ3MpO1xuXHRcdFx0fTtcblx0XHR9LFxuXHRcdHJhbmQgPSBmdW5jdGlvbigpe1xuXHRcdFx0cmV0dXJuICgnMDAwMDAnKyhNYXRoLnJhbmRvbSgpKjE2Nzc3MjE2PDwwKS50b1N0cmluZygxNikpLnN1YnN0cigtNikudG9VcHBlckNhc2UoKTtcblx0XHR9O1xuXHRcblx0JC5mbi5lYXN5RHJvcERvd24gPSBmdW5jdGlvbigpe1xuXHRcdHZhciBhcmdzID0gYXJndW1lbnRzLFxuXHRcdFx0ZGF0YVJldHVybiA9IFtdLFxuXHRcdFx0ZWFjaFJldHVybjtcblx0XHRcdFxuXHRcdGVhY2hSZXR1cm4gPSB0aGlzLmVhY2goZnVuY3Rpb24oKXtcblx0XHRcdGlmKGFyZ3MgJiYgdHlwZW9mIGFyZ3NbMF0gPT09ICdzdHJpbmcnKXtcblx0XHRcdFx0dmFyIGRhdGEgPSBFYXN5RHJvcERvd24ucHJvdG90eXBlLmluc3RhbmNlc1t0aGlzLmlkXVthcmdzWzBdXShhcmdzWzFdLCBhcmdzWzJdKTtcblx0XHRcdFx0aWYoZGF0YSlkYXRhUmV0dXJuLnB1c2goZGF0YSk7XG5cdFx0XHR9IGVsc2Uge1xuXHRcdFx0XHRpbnN0YW50aWF0ZSh0aGlzLCBhcmdzWzBdKTtcblx0XHRcdH07XG5cdFx0fSk7XG5cdFx0XG5cdFx0aWYoZGF0YVJldHVybi5sZW5ndGgpe1xuXHRcdFx0cmV0dXJuIGRhdGFSZXR1cm4ubGVuZ3RoID4gMSA/IGRhdGFSZXR1cm4gOiBkYXRhUmV0dXJuWzBdO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRyZXR1cm4gZWFjaFJldHVybjtcblx0XHR9O1xuXHR9O1xuXHRcblx0JChmdW5jdGlvbigpe1xuXHRcdGlmKHR5cGVvZiBPYmplY3QuZ2V0UHJvdG90eXBlT2YgIT09ICdmdW5jdGlvbicpe1xuXHRcdFx0aWYodHlwZW9mICd0ZXN0Jy5fX3Byb3RvX18gPT09ICdvYmplY3QnKXtcblx0XHRcdFx0T2JqZWN0LmdldFByb3RvdHlwZU9mID0gZnVuY3Rpb24ob2JqZWN0KXtcblx0XHRcdFx0XHRyZXR1cm4gb2JqZWN0Ll9fcHJvdG9fXztcblx0XHRcdFx0fTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdE9iamVjdC5nZXRQcm90b3R5cGVPZiA9IGZ1bmN0aW9uKG9iamVjdCl7XG5cdFx0XHRcdFx0cmV0dXJuIG9iamVjdC5jb25zdHJ1Y3Rvci5wcm90b3R5cGU7XG5cdFx0XHRcdH07XG5cdFx0XHR9O1xuXHRcdH07XG5cdFx0XG5cdFx0JCgnc2VsZWN0LmRyb3Bkb3duJykuZWFjaChmdW5jdGlvbigpe1xuXHRcdFx0dmFyIGpzb24gPSAkKHRoaXMpLmF0dHIoJ2RhdGEtc2V0dGluZ3MnKTtcblx0XHRcdFx0c2V0dGluZ3MgPSBqc29uID8gJC5wYXJzZUpTT04oanNvbikgOiB7fTsgXG5cdFx0XHRpbnN0YW50aWF0ZSh0aGlzLCBzZXR0aW5ncyk7XG5cdFx0fSk7XG5cdH0pO1xufSkoalF1ZXJ5KTsiXSwiZmlsZSI6ImpxdWVyeS5lYXN5ZHJvcGRvd24uanMifQ==
